package topostfix;
import java.util.ArrayList;
import controller.VerbVisTrial;
import visual.GuiDeclarations;

public class QueueHasStack<Item> {
	 private Stack<Item> temp;
	 private Stack<Item> value;
	 private ArrayList<Item> lsMainList;
	 private ArrayList<Item> lsMainTempList ;
	 private ArrayList<Item> lsTempList;
	 private ArrayList<Item> lsTempTempList;
	 private StringBuffer upperValueBuf;
	 private StringBuffer upperTempBuf;
	 private StringBuffer stackMarker;
	 private StringBuffer igbaw;
	 private Item tempPop;
	 private VerbVisTrial trialKo;
	 private GuiDeclarations guiDecQ;
	 
	 public QueueHasStack(VerbVisTrial trialKo, GuiDeclarations guiDecQ, StringBuffer igbaw){	
		 upperValueBuf = new StringBuffer();
		 upperTempBuf = new StringBuffer();
		 upperValueBuf.append("Main Stack");
		 upperTempBuf.append("Temp Stack");
		 temp = new Stack<Item>(trialKo,guiDecQ,upperTempBuf,igbaw);
		 value = new Stack<Item>(trialKo,guiDecQ,upperValueBuf,igbaw);
		 this.igbaw = igbaw;
		 this.trialKo = trialKo;
		 this.guiDecQ = guiDecQ;
		 initializeArrayLists();
		 initializeBuffers();
	 }
	 /*This tells on which queue 
	  * the data should be put.
	  * */
	 public void setUp(String str){
		 igbaw = new StringBuffer();
		 igbaw.append(str);
	 }
	 /*This tells on which lower stack
	  * should the data be stored.
	  * */
	 public void setStackMarker(String str){
		 stackMarker = new StringBuffer();
		 stackMarker.append(str);
	 }
	 /*If main queue is empty, data is put in main stack.
	  * Else transfer everything to temp Stack and
	  * push data to empty main stack. Then transfer everything
	  * all the data from the temp stack to main stack again.
	  * */
	 public void enqueue(Item x) {
		 if(value.isEmpty()){
		 	 value.push(x);
		 	 if(igbaw.toString().equals("Main Queue") && stackMarker.toString().equals("to Main")){
		 		 value.setUpStackBuf("Main Stack");
	             value.setIgbaw("Main Queue");
	             value.pushGUI();
	             lsMainList.add(value.peek());
	             guiDecQ.makeTextField(1, lsMainList.get(0).toString(), "ls");
		         printIt( "pushed ", lsMainList.get(0).toString(), " to Main Stack of Main Queue");
		         trialKo.delay(trialKo.getDelayTime());
	            
	         }else if(igbaw.toString().equals("Temp Queue") && stackMarker.toString().equals("to Main")){
	        	 value.setUpStackBuf("Temp Stack");
	        	 value.setIgbaw("Temp Queue");
	        	 value.pushGUI();
	        	 lsTempList.add(value.peek());
	        	 guiDecQ.makeTextField(3, lsTempList.get(0).toString(), "ls");
	        	 trialKo.delay(trialKo.getDelayTime());
	        	 printIt( "pushed ", lsTempList.get(0).toString(), " to Main stack of temp queue");
	        	 trialKo.delay(trialKo.getDelayTime());
	        	    
	         }   
		 }else{
			 fromMainToTemp();
	         value.push(x);
	         if(stackMarker.toString().equals("to Main")){
	        	 if(igbaw.toString().equals("Main Queue") ){
	        		 value.setUpStackBuf("Main Stack");
	        		 value.setIgbaw("Main Queue");
	        		 value.pushGUI();
	        		 lsMainList.add(value.peek());
	       			 guiDecQ.makeTextField(1, lsMainList.get(0).toString(), "ls");
	        		 printIt("pushed ", lsMainList.get(0).toString(), " to lsValue of Main Queue");
	       			 trialKo.delay(trialKo.getDelayTime()); 
	        	
	        	}else if(igbaw.toString().equals("Temp Queue") ){

	        		value.setUpStackBuf("Temp Stack");
	        		value.setIgbaw("Temp Queue");
	        		value.pushGUI();
	        		lsTempList.add(value.peek());
	        		guiDecQ.makeTextField(3, lsTempList.get(0).toString(), "ls");
	        		printIt("pushed ", lsTempList.get(0).toString(), " to lsValue of Temp Queue");
	        		trialKo.delay(trialKo.getDelayTime());
	            	
	        	}
	         }
		 	fromTempToMain();
		 }
	 }
	 /*This pops the datum stored in main stack.
	  * */
	 public Item dequeue() {
		 if(stackMarker.toString().equals("to Main")){
			 if(igbaw.toString().equals("Main Queue") ){
				 value.setUpStackBuf("Main Stack");
				 value.setIgbaw("Main Queue");
				 if(lsMainList.size()>0){
					 guiDecQ.removeTextField(1, "ls");
					 guiDecQ.changePosition(1, "ls",'-');
					 printIt( "pop ", lsMainList.get(lsMainList.size()-1).toString(), " in lsValue of Main Queue");
					 lsMainList.remove(lsMainList.size()-1);
					 trialKo.delay(trialKo.getDelayTime());
				 }else{
					 printIt( "nothing to pop "," ", " in lsValue");
					 trialKo.delay(trialKo.getDelayTime());
				 }
			 }else if(igbaw.toString().equals("Temp Queue") ){
				 value.setUpStackBuf("Temp Stack");
				 value.setIgbaw("Temp Queue");
				 if(lsTempList.size()> 0){
					 guiDecQ.removeTextField(3, "ls");
					 guiDecQ.changePosition(3, "ls",'-');
					 printIt( "popping ", lsTempList.get(lsTempList.size()-1).toString(), " in lsValue of Temp Queue");
					 lsTempList.remove(lsTempList.size()-1);
					 trialKo.delay(trialKo.getDelayTime());
	    		 }else{
	    			 printIt( "nothing to pop "," ", " in lsValue");
	    			 trialKo.delay(trialKo.getDelayTime());
	    		}	
			 }
		 }	
		 if(!temp.isEmpty()){
			 fromTempToMain();
		 }
		 return value.pop();    
	 }
	 /*returns the popped value of main
	  * stack.
	  * */
	 public Item getPopped(){
	 	return value.getPopped();
	 }
	 @SuppressWarnings("unchecked")
	 public Item peek() {
		 if(value.peek().equals(null)){
			 return ((Item) " ");
		 }else{
			 return value.peek();
		 }
	  }
	 public void fromMainToTemp(){
		 while(!value.isEmpty()){
			 if(stackMarker.toString().equals("to Main")){//to main stack
				 if(igbaw.toString().equals("Main Queue") ){
					 value.setUpStackBuf("Main Stack");
					 value.setIgbaw("Main Queue");
					 if(lsMainList.size()>0){		
						 guiDecQ.removeTextField(1, "ls");
						 guiDecQ.changePosition(1, "ls",'-');
						 printIt( "popping ", lsMainList.get(lsMainList.size()-1).toString(), " in Main lower Stack of Main Queue");
						 lsMainList.remove(lsMainList.size()-1);
						 trialKo.delay(trialKo.getDelayTime());
            		 }else{
            			 printIt( "popping nothing"," ", " in Main lower Stack of Main Queue");
            			 trialKo.delay(trialKo.getDelayTime());
            		 }
            	 }else if(igbaw.toString().equals("Temp Queue") ){
            		 value.setUpStackBuf("Temp Stack");
            		 value.setIgbaw("Temp Queue");
            		 if(lsTempList.size()>0){
            			 guiDecQ.removeTextField(3, "ls");
            			 guiDecQ.changePosition(3, "ls",'-');
            			 printIt( "popping ", lsTempList.get(lsTempList.size()-1).toString(), " in lsValue of Temp Queue");
            			 lsTempList.remove(lsTempList.size()-1);
            			 trialKo.delay(trialKo.getDelayTime());
            		 } else{
            			 printIt( "popping nothing ", " ", " in lsValue of Temp Queue");
            			 trialKo.delay(trialKo.getDelayTime());
            		}	
            	 }		
			 }
         tempPop = value.pop();
         temp.push(tempPop);
         if(stackMarker.toString().equals("to Temp")){
        	 if(igbaw.toString().equals("Main Queue") ){
        		 temp.setUpStackBuf("MainTemp Stack");
        		 temp.setIgbaw("Main Queue");
        		 temp.pushGUI();
        		 lsMainTempList.add(temp.peek());
        		 if(lsMainTempList.size()>1){
        			 guiDecQ.changePosition(2, "ls",'+');
        			 guiDecQ.makeTextField(2, lsMainTempList.get(lsMainTempList.size()-1).toString(), "ls");
        			 printIt( "pushed ", lsMainTempList.get(lsMainTempList.size()-1).toString(), " to ls Temp of Main Queue");
        		 }else{
        			 guiDecQ.makeTextField(2, lsMainTempList.get(0).toString(), "ls");
        			 printIt( "pushed ", lsMainTempList.get(0).toString(), " to ls Temp of Main Queue");
        			 trialKo.delay(trialKo.getDelayTime());
        		 }
        	 }else if(igbaw.toString().equals("Temp Queue") ){
        		 temp.setUpStackBuf("TempTemp Stack");
        		 temp.setIgbaw("Temp Queue");
        		 temp.pushGUI();
        		 lsTempTempList.add( temp.peek());
        		 if(lsTempTempList.size()>1){
        			 guiDecQ.changePosition(4, "ls",'+');
        			 guiDecQ.makeTextField(4, lsTempTempList.get(lsTempTempList.size()-1).toString(), "ls");	
        			 printIt( "pushed ", lsTempTempList.get(lsTempTempList.size()-1).toString(), " to lsTemp of Temp Queue");
        			 trialKo.delay(trialKo.getDelayTime());
        		 }else{
        			 guiDecQ.makeTextField(4, lsTempTempList.get(0).toString(), "ls");
        			 printIt( "pushed ", lsTempTempList.get(0).toString(), " to lsTemp of Temp Queue");
        			 trialKo.delay(trialKo.getDelayTime());	
        		 }
        	  }
         	}
		 }
	 }
	 public void fromTempToMain(){
		 while(!temp.isEmpty()){
        	 if(stackMarker.toString().equals("to Temp")){
        		 if(igbaw.toString().equals("Main Queue")){
        			 temp.setUpStackBuf("MainTemp Stack");
        			 temp.setIgbaw("Main Queue");
        			 if(lsMainTempList.size() > 0){
        				 guiDecQ.removeTextField(2, "ls");
        				 guiDecQ.changePosition(2, "ls",'-');
        				 printIt( "popping ", lsMainTempList.get(lsMainTempList.size()-1).toString(), " in lsTemp of Main Queue");
        				 lsMainTempList.remove(lsMainTempList.size()-1);
        				 trialKo.delay(trialKo.getDelayTime());
        			 }else{
        				 printIt( "popping nothing ", " ", " in lsTemp of Main Queue");
        				 trialKo.delay(trialKo.getDelayTime());
        			 }
        		 }else if (igbaw.toString().equals("Temp Queue")){
        			 temp.setUpStackBuf("TempTemp Stack");
        			 temp.setIgbaw("Temp Queue");
        			 if(lsTempTempList.size()>0 ){ 
        				 guiDecQ.removeTextField(4, "ls");
        				 guiDecQ.changePosition(4, "ls",'-');
        				 printIt( "popping ", lsTempTempList.get(lsTempTempList.size()-1).toString(), " in lsTemp of Temp Queue");
        				 lsTempTempList.remove(lsTempTempList.size()-1);
        				 trialKo.delay(trialKo.getDelayTime());		
        			 }else{
        				 printIt( "popping nothing ","", " in lsTemp of Temp Queue");
        				 trialKo.delay(trialKo.getDelayTime());
        			 }
        		 } 
        	 }
        	 tempPop = temp.pop();
        	 value.push(tempPop);
        	 if(stackMarker.toString().equals("to Main")){
        		 if(igbaw.toString().equals("Main Queue") ){
        			 value.setUpStackBuf("Main Stack");
        			 value.setIgbaw("Main Queue");
        			 value.pushGUI();
        			 lsMainList.add(value.peek());
        			 if(lsMainList.size()>0){
        				 guiDecQ.changePosition(1, "ls",'+');
        				 guiDecQ.makeTextField(1,lsMainList.get(lsMainList.size()-1).toString(), "ls");
        				 printIt( "pushed ", lsMainList.get(lsMainList.size()-1).toString(), " in lsValue");
        				 trialKo.delay(trialKo.getDelayTime());
        			 }else{
        				 guiDecQ.makeTextField(1,lsMainList.get(0).toString(), "ls");
        				 printIt( "pushed ", lsMainList.get(0).toString(), " in lsValue");
        				 trialKo.delay(trialKo.getDelayTime());
        			 }
        		 }else if (igbaw.toString().equals("Temp Queue")){
        			 value.setUpStackBuf("Temp Stack");
        			 value.setIgbaw("Temp Queue");	
        			 value.pushGUI();
        			 lsTempList.add(value.peek());
        			 if(lsTempList.size()>0){
        				 guiDecQ.changePosition(3, "ls",'+');
        				 guiDecQ.makeTextField(3, lsTempList.get(lsTempList.size()-1).toString(), "ls");
        				 printIt("pushed ", lsTempList.get(lsTempList.size()-1).toString(), "in lsvalue");
        				 trialKo.delay(trialKo.getDelayTime());	
        			 }else{
        				 guiDecQ.makeTextField(3, lsTempList.get(0).toString(), "ls");
        				 printIt("pushed ", lsTempList.get(0).toString(), "in lsvalue");
        				 trialKo.delay(trialKo.getDelayTime());
        			 }
        		 }
        	 }
		 }
	 }
	 /*
	  * returns a boolean if 
	  * this queue is empty or not.
	  * */
	 public boolean empty() {
		 return value.isEmpty();
	 }
	 /*
	  * returns the length or size
	  * of this queue.
	  * */
	 public int length(){
		 return value.length();
	 }
	 public void printIt( String message, String buffer, String destination){
		 trialKo.logJTA.append(message + buffer + destination + VerbVisTrial.newline);
		 trialKo.logJTA.setCaretPosition(trialKo.logJTA.getDocument().getLength());	
	 }

	 public void initializeArrayLists(){
		 lsMainList = new ArrayList<Item>();
		 lsMainTempList = new ArrayList<Item>();
		 lsTempList = new ArrayList<Item>();
		 lsTempTempList = new ArrayList<Item>();
			
	 }
	 public void initializeBuffers(){
		 stackMarker = new StringBuffer();
	 }
}
