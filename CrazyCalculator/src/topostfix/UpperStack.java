package topostfix;


import java.util.ArrayList;
import java.util.NoSuchElementException;

import controller.VerbVisTrial;
import visual.GuiDeclarations;

public class UpperStack<Item>{
	private ArrayList<Item> qMainList;
	private ArrayList<Item> qTempList;
	private QueueHasStack<Item> sulod ;
	private QueueHasStack<Item> sulodTmp ;
	private StringBuffer sulodMarker;
	private StringBuffer tempMarker;
	private VerbVisTrial trial;
	private GuiDeclarations guiNorm;

   
    public UpperStack(VerbVisTrial trial, GuiDeclarations guiNorm){
    	initializeBuffer();
        initializeArrayList();
    	this.trial = trial;
    	this.guiNorm = guiNorm;
    	sulodMarker.append("Main Queue");
    	tempMarker.append("Temp Queue");
    	sulod = new QueueHasStack<Item>(trial, guiNorm,sulodMarker);
        sulodTmp = new QueueHasStack<Item>(trial,guiNorm,tempMarker);  
    }    
 
    public void push(Item data){
        if (sulod.length() == 0){
        	sulod.setUp("Main Queue");
        	sulod.setStackMarker("to Main");
            sulod.enqueue(data);
            qMainList.add(sulod.peek());
         
           	guiNorm.makeTextField(1, qMainList.get(0).toString(), "q");
           	printIt("enqueued ", qMainList.get(0).toString(), " to QMain that's empty");
           	trial.delay(trial.getDelayTime());    
        }	
        else{     /*if main queue isn't empty, put datum here.*/
        	fromMainToTemp();
         
            sulod.setUp("Main Queue");
        	sulod.setStackMarker("to Main");
            sulod.enqueue(data);
            qMainList.add(sulod.peek());
            if(qMainList.size() > 0){
            	guiNorm.changePosition(1, "q", '+');
            	guiNorm.makeTextField(1, qMainList.get(qMainList.size()-1).toString(), "q");
                printIt("enqueued ", qMainList.get(qMainList.size()-1).toString(), " to Qmain");
                trial.delay(trial.getDelayTime());
            }else{
            	 guiNorm.makeTextField(1, qMainList.get(0).toString(), "q");
                 printIt("enqueued ", qMainList.get(0).toString(), " to Qmain");
                 trial.delay(trial.getDelayTime());
            }
            /*pop data in temp queue before before putting it to main q*/
           fromTempToMain();
        }
    }    
    public Item pop(){
        if (sulod.length() == 0)
            throw new NoSuchElementException("Underflow Exception po."); 
        sulod.setUp("Main Queue");
        sulod.setStackMarker("to Main");
        if(qMainList.size()>0){
        		guiNorm.removeTextField(1, "q");
        		guiNorm.changePosition(1, "q", '-');
               	printIt( "popped ", qMainList.get(0).toString(), " at Main Queue");//qMainList.size()-1
               	qMainList.remove(0);//qMainList.size() - 1
               	trial.delay(trial.getDelayTime());
        }else{
        	printIt( "nothing to pop ", " ", " at Main Queue");
        	trial.delay(trial.getDelayTime());
        }
        if(!sulodTmp.empty()){
        	 fromTempToMain();
        }
        return sulod.dequeue();
    }
    public void fromMainToTemp(){
    	int j = sulod.length();
        for (int i = 0; i < j ; i++){
        	sulod.setUp("Main Queue");
            sulod.setStackMarker("to Main");
        	if(qMainList.size()>0){
        			guiNorm.removeTextField(1, "q");
        			guiNorm.changePosition(1, "q", '-');
            		printIt("dequeued ", qMainList.get(qMainList.size()-1).toString(), " in QMain");//0
            		qMainList.remove(qMainList.size()-1);//0
            		trial.delay(trial.getDelayTime());	
        	}else{
        			printIt("dequeued nothing ", " ", " in QMain");
        			trial.delay(trial.getDelayTime());
        	}
        	sulodTmp.setUp("Temp Queue");
            sulodTmp.setStackMarker("to Main");
        	sulodTmp.enqueue(sulod.dequeue());  
        	qTempList.add(sulod.getPopped());//
        	if(qTempList.size()>0){
        		guiNorm.changePosition(2, "q",'+');
        		guiNorm.makeTextField(2, qTempList.get(qTempList.size()-1).toString(), "q");
        		printIt("enqueued ", qTempList.get(qTempList.size() - 1).toString(), " to QTemp");
        		trial.delay(trial.getDelayTime());
        	}else{
        		guiNorm.makeTextField(2, qTempList.get(0).toString(), "q");
        		printIt("enqueued ", qTempList.get(0).toString(), " to QTemp");
        		trial.delay(trial.getDelayTime());
        	}
        }    
    }
    public void fromTempToMain(){
    	  for (int i = 0; i < sulodTmp.length(); i++){
          	sulodTmp.setUp("Temp Queue");
          	sulodTmp.setStackMarker("to Main");
          	if(qTempList.size()>0){
          		guiNorm.removeTextField(2, "q");
          		guiNorm.changePosition(2, "q", '-');
          		printIt("dequeued ", qTempList.get(0).toString(), " to QTemp");
          		qTempList.remove(0);
          		trial.delay(trial.getDelayTime());
          	}else{
          		printIt("dequeued nothing", " ", " to QTemp");
          		trial.delay(trial.getDelayTime());
          	}	
 
          	sulod.setUp("Main Queue");
          	sulod.setStackMarker("to Main");
          	sulod.enqueue(sulodTmp.dequeue());
          	qMainList.add(sulodTmp.getPopped());
          	if(qMainList.size()>0){
          		guiNorm.changePosition(1, "q", '+');
          		guiNorm.makeTextField(1, qMainList.get(qMainList.size()-1).toString(), "q");
              	printIt( "enqueued ", qMainList.get(qMainList.size()-1).toString(), " to QMain");
              	trial.delay(trial.getDelayTime());
          	}else{
          		guiNorm.makeTextField(1, qMainList.get(0).toString(), "q");
              	printIt( "enqueued ", qMainList.get(0).toString(), " to QMain");
              	trial.delay(trial.getDelayTime());
          	}
          }
    }
    public void printIt( String message, String buffer, String destination){
    	  trial.logJTA.append( message + buffer + destination + VerbVisTrial.newline);
    	  trial.logJTA.setCaretPosition(trial.logJTA.getDocument().getLength());
    }
   
    public Item peek(){
        if (sulod.length() == 0)
            throw new NoSuchElementException("Underflow Exception po.");            
        return sulod.peek();
    }        
    public boolean isEmpty(){
        return sulod.length() == 0 ;
    }
    public void initializeArrayList(){
    	qMainList = new ArrayList<Item>();
		qTempList = new ArrayList<Item>();
    }
    public void initializeBuffer(){
 
    	sulodMarker = new StringBuffer();
    	tempMarker = new StringBuffer();
    }
   
    public int getSize(){
        return sulod.length();
    }    
	
	
   
}
