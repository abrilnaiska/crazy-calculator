package topostfix;
import java.util.ArrayList;

import controller.VerbVisTrial;
import visual.GuiDeclarations;

public class Stack<Item> {
	private Node first = null;
	private ArrayList<Item> udsMainList;
	private ArrayList<Item> udsMainTempList ;
	private ArrayList<Item> udsTempList;
	private ArrayList<Item> udsTempTempList ;
	private VerbVisTrial trial;
	private GuiDeclarations guiDec;
	private StringBuffer upStackBuf;
	private StringBuffer trueIgbaw;
	private Item poppedItem;
	private class Node{
		Item item;
		Node next;
	}
	public Stack(VerbVisTrial trial, GuiDeclarations guiDec, StringBuffer upStackBuf,StringBuffer trueIgbaw){
		this.trial = trial;
		this.guiDec = guiDec;
		initializeArrayLists();
		this.upStackBuf = upStackBuf;
		this.trueIgbaw = trueIgbaw;
	}
	
	public boolean isEmpty(){
		return first == null;
	}
	public void setUpStackBuf(String str){
		upStackBuf = new StringBuffer();
		upStackBuf.append(str);
	}
	public void setIgbaw(String str){
		trueIgbaw = new StringBuffer();
		trueIgbaw.append(str);
	}
	public void pushGUI(){
		if(upStackBuf.toString().equals("Main Stack") && trueIgbaw.toString().equals("Main Queue")){
			udsMainList.add(this.peek());
			if(udsMainList.size()>1){
				guiDec.changePosition(1, "uds", '+');
				guiDec.makeTextField(1, udsMainList.get(udsMainList.size()-1).toString(),"uds");
				guiDec.makeLink(true, false, 1);
				printIt("pushed ", udsMainList.get(udsMainList.size()-1).toString(), " to node of Main Q's uds Value stack");
				trial.delay(trial.getDelayTime());
			}else{
				guiDec.makeTextField(1, udsMainList.get(0).toString(),"uds");
				guiDec.makeLink(false, false, 1);
				printIt("pushed ",udsMainList.get(0).toString() , " to node of Main Q's uds Value Stack");
				trial.delay(trial.getDelayTime());
			}
			
		}
		else if(upStackBuf.toString().equals("Temp Stack") && trueIgbaw.toString().equals("Temp Queue")){
			udsTempList.add(this.peek());
			if(udsTempList.size()>1){
				guiDec.changePosition(3, "uds", '+');
				guiDec.makeTextField(3, udsTempList.get(udsTempList.size()-1).toString(),"uds");
				guiDec.makeLink(true, false, 3);
				printIt("pushed ", udsTempList.get(udsTempList.size()-1).toString(), " to node of Temp Q's uds main stack");
				trial.delay(trial.getDelayTime());
			}else {
				guiDec.makeTextField(3, udsTempList.get(0).toString(),"uds");
				guiDec.makeLink(false, false, 3);
				printIt("pushed ", udsTempList.get(0).toString(), " to node of Temp Q's uds main stack");
				trial.delay(trial.getDelayTime());
			}
		}else if(upStackBuf.toString().equals("MainTemp Stack") && trueIgbaw.toString().equals("Main Queue")){
			udsMainTempList.add(this.peek());
			if(udsMainTempList.size()>1){
				guiDec.changePosition(2, "uds", '+');
				guiDec.makeTextField(2, udsMainTempList.get(udsMainTempList.size()-1).toString(),"uds");
				guiDec.makeLink(true, false, 2);
				printIt("pushed ", udsMainTempList.get(udsMainTempList.size()-1).toString(), " to node of Main Q's uds temp stack");
				trial.delay(trial.getDelayTime());
			}else{
				guiDec.makeTextField(2, udsMainTempList.get(0).toString(),"uds");
				guiDec.makeLink(false, false, 2);
				printIt("pushed  ",udsMainTempList.get(0).toString(), " to node of Main Q's uds temp Stack");
				trial.delay(trial.getDelayTime());
			}
		}else if(upStackBuf.toString().equals("TempTemp Stack") && trueIgbaw.toString().equals("Temp Queue")){
			udsTempTempList.add(this.peek());
			if(udsTempTempList.size()>1){
				guiDec.changePosition(4, "uds", '+');
				guiDec.makeTextField(4, udsTempTempList.get(udsTempTempList.size()-1).toString(),"uds");
			
				guiDec.makeLink(true, false, 4);
				printIt("pushed ", udsTempTempList.get(udsTempTempList.size()-1).toString(), " to node of Temp Q's uds temp stack");
				trial.delay(trial.getDelayTime());
			}else {
				guiDec.makeTextField(4, udsTempTempList.get(0).toString(),"uds");
				guiDec.makeLink(false, false, 4);
				printIt("pushed ", udsTempTempList.get(0).toString(), " to node of Temp Q's uds temp stack");
				trial.delay(trial.getDelayTime());
			}	
		}
	}
	public void popGUI(){
		if(trueIgbaw.toString().equals("Main Queue") && upStackBuf.toString().equals("Main Stack")){
			if(udsMainList.size() >1){
				guiDec.removeTextField(1, "uds");
				guiDec.changePosition(1, "uds", '-');
				printIt("popped ", udsMainList.get(udsMainList.size()-1).toString(), " in node of Main Stack in Main Queue");
				udsMainList.remove(udsMainList.size()-1);
				if(udsMainList.size() == 1){
					guiDec.setLinkText(1, 1);
				}else{
					guiDec.setLinkText(2, 1);
				}
				trial.delay(trial.getDelayTime());
			}else if(udsMainList.size() == 1){
				//guiDec.setLinkText(1, 1);
				guiDec.removeTextField(1, "uds");
				guiDec.changePosition(1, "uds", '-');
				printIt("popped ", udsMainList.get(udsMainList.size()-1).toString(), " in node of Main Stack in Main Queue");
				udsMainList.remove(udsMainList.size()-1);
				trial.delay(trial.getDelayTime());
			}
			else{
				printIt("popped nothing", " ", "in node of Main Stack in Main Queue");
				trial.delay(trial.getDelayTime());
			}
		}else if(trueIgbaw.toString().equals("Main Queue") && upStackBuf.toString().equals("MainTemp Stack")){
			if(udsMainTempList.size() >1){
				guiDec.removeTextField(2, "uds");
				guiDec.changePosition(2, "uds", '-');
				if(udsMainTempList.size() == 1){
					guiDec.setLinkText(1, 2);
				}else{
					guiDec.setLinkText(2, 2);
				}
				printIt("popped ", udsMainTempList.get(udsMainTempList.size()-1).toString(), " in node of Temp stack in Main Queue");
				udsMainTempList.remove(udsMainTempList.size()-1);
				trial.delay(trial.getDelayTime());
			}else if(udsMainTempList.size() == 1){
				//guiDec.setLinkText(1, 2);
				guiDec.removeTextField(2, "uds");
				guiDec.changePosition(2, "uds", '-');
				printIt("popped ", udsMainTempList.get(udsMainTempList.size()-1).toString(), " in node of Temp stack in Main Queue");
				udsMainTempList.remove(udsMainTempList.size()-1);
				trial.delay(trial.getDelayTime());
			}else{
				printIt("nothing to pop", " ", "in node of Temp  Stack in Main Queue");
				trial.delay(trial.getDelayTime());
			}
		}else if(trueIgbaw.toString().equals("Temp Queue") && upStackBuf.toString().equals("Temp Stack")){
			if(udsTempList.size() > 1){
				guiDec.removeTextField(3, "uds");
				guiDec.changePosition(3, "uds", '-');
				if(udsTempList.size() == 1){
					guiDec.setLinkText(1, 3);
				}else{
					guiDec.setLinkText(2, 3);
				}
				printIt("popped ", udsTempList.get(udsTempList.size()-1).toString(), " in node of Main Stack in Temp Queue");
				udsTempList.remove(udsTempList.size()-1);
				trial.delay(trial.getDelayTime());
			}else if(udsTempList.size() == 1){
				//guiDec.setLinkText(1, 3);
				guiDec.removeTextField(3, "uds");
				guiDec.changePosition(3, "uds", '-');
				printIt("popped ", udsTempList.get(udsTempList.size()-1).toString(), " in node of Main Stack in Temp Queue");
				udsTempList.remove(udsTempList.size()-1);
				trial.delay(trial.getDelayTime());
			}else{
				printIt("nothing to pop", " ", "in node of Main Stack in Temp Queue");
				trial.delay(trial.getDelayTime());
			}
		}else if(trueIgbaw.toString().equals("Temp Queue") && upStackBuf.toString().equals("TempTemp Stack")){
			if(udsTempTempList.size() > 1){
				guiDec.removeTextField(4, "uds");
				guiDec.changePosition(4, "uds", '-');
				if(udsTempTempList.size() == 1){
					guiDec.setLinkText(1, 4);
				}else{
					guiDec.setLinkText(2, 4);
				}
				printIt("pop ", udsTempTempList.get(udsTempTempList.size()-1).toString(), " in node of Temp Stack in Temp Queue");
				udsTempTempList.remove(udsTempTempList.size()-1);
				trial.delay(trial.getDelayTime());
			}else if(udsTempTempList.size() == 1){
				//guiDec.setLinkText(1, 4);
				guiDec.removeTextField(4, "uds");
				guiDec.changePosition(4, "uds", '-');
				printIt("pop ", udsTempTempList.get(udsTempTempList.size()-1).toString(), " in node of Temp Stack in Temp Queue");
				udsTempTempList.remove(udsTempTempList.size()-1);
				trial.delay(trial.getDelayTime());
			}else{
				printIt("nothing to pop", " ", "in node of Temp Stack in Temp Queue");
				trial.delay(trial.getDelayTime());
			}
		}
	}
	/*
	 * iton previous adto ha last.
	 * tas an newly added adto ha first.
	 * */
	public void push(Item item){
		Node oldFirst = first;
		first = new Node();
		first.item = item;
		first.next = oldFirst;	
	}
	/*
	 * pops the recently added item.
	 * */
	public Item pop(){
		Item item = first.item;
		poppedItem = first.item;
		popGUI();
		first = first.next;
		return item;
	}
	/*
	 * sees the last item
	 * recently added.
	 * */
	public Item peek(){
		return first.item;
	}
	public Item getPopped(){
		return poppedItem;
	}
	public void adjustText(ArrayList<Item> arrList){
		if(arrList.size() == 1){//null it pointer
			
		}else{
			
		}
	}
	public int length(){
		int count=0;
		Node current = this.first;
		while(current != null){
			current=current.next;
			++count;
		} 
		return count;
	}
	
	 public void printIt( String message, String buffer, String destination){
		 	trial.logJTA.append(message + buffer + destination + VerbVisTrial.newline);
		 	trial.logJTA.setCaretPosition(trial.logJTA.getDocument().getLength()); 	
	}
	public void initializeArrayLists(){
		udsMainList = new ArrayList<Item>();
		udsMainTempList = new ArrayList<Item>();
		udsTempList = new ArrayList<Item>();
		udsTempTempList = new ArrayList<Item>();
	}
	
}
