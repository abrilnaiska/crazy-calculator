package controller;

public class PostfixFunctions {
	private static final char ADD = '+', SUBTRACT = '-';
	private static final char MULTIPLY = '*', DIVIDE = '/';
	private static final char EXPONENTIATE = '^';
	public boolean isOper(char sign) { 
		return sign == '('  ||  sign == ')'  ||  sign == '^'  ||  sign == '*'  ||  sign == '/'|| sign == '+' || sign == '-';
	}
	public boolean isSpace(char input) {  
	    return (input == ' ');
	}
	public boolean lowerPrec(char opLeft, char opRight) {
		switch (opLeft) {
			case '^':
		      	return opRight == '(' || opRight == ')';
		    case '*':
		    	 return opRight == '^' || opRight == '(' || opRight == ')';
		    case '/':
		        return opRight == '^' || opRight == '(' || opRight == ')' || opRight == '*';
		    case '+':
		    case '-':
		        return !(opRight == '+' || opRight == '-') ;
		    case '(': return true;
		    default:
		        return false;
		}
   } 
	public int evalSingleOp(char oper, int first, int second) {
        int result = 0;
        switch (oper) {
            case SUBTRACT :
                result = first - second;
                break;
            case EXPONENTIATE :
            	result = (int) Math.pow(first,second);
            	break;
            case MULTIPLY :
                result = first * second;
                break;
            case DIVIDE :
            	result = (first / second);
            	break;
            case ADD :
                result = first + second;
                break;
        }

        return result;
    }
	public boolean isOperator(String oper) {
        return (oper.equals("+") || oper.equals("-") || oper.equals("*") || oper.equals("/") || oper.equals("^"));
    }
}
