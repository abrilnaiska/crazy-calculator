package controller;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import topostfix.UpperStack;
import visual.GuiDeclarations;
import visual.View;

public class VerbVisTrial extends JPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static String newline = "\n";
	public JButton back;
	@SuppressWarnings("unused")
	private View view;
	private ImageIcon aboutImage;
	private UpperStack<String> operatorStack;  
	private UpperStack<Integer> evaluateStack;
	private ArrayList<String> upStackOperatorList;
	private ArrayList<Integer> upStackNumberList;
	private String expression;
	private boolean done;
	private char justAChar;
	private StringTokenizer parser ;
	private StringTokenizer parser2 ;
    private StringBuffer postfix ;
    private StringBuffer resultBuffer;
    private StringBuffer parsed; 
    private StringBuffer evalParsed;
    private int op1, op2;
    private int result = 0;
    private String token;
    private static boolean stillRunning;
    private Thread thread;
    private GuiDeclarations gui;
    private PostfixFunctions postFunctions;
    public JTextArea logJTA;
    public JTextField enterTF;
    private int delayTime;
    /*
     * Thread's run method.
     * */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(stillRunning){
			introCommandLine();
			convertToPostfix(enterTF.getText());
			optionPaneForLog();
			optionPaneForEval("Enter desired delay for eval (1000 = 1 sec):");
			printIt("--------------------Evaluation Process---------------");
			gui.putEnterTF.setText(postfix.toString());
			gui.putCurPostFixTF.setText(null);
			evaluate(postfix.toString());
			printIt("Evaluation is finished.Go back to menu to start again.");
			stop();
		}
	}
	public VerbVisTrial(View view){	
		this.view = view;
		setLayout(null);
		setBounds(0,0,800,600);
		declarations();
		visual();
		setComponentTextNull();
	}
	/*
	 * Short message in the log 
	 * history during intro.
	 * */
	public void introCommandLine(){
		printIt("               /////////////*****WELCOME DEAR USER*****//////////////" );
		delay(500);
		printIt("Notes: 1. Assumed top of stacks is on the right." );
		delay(500);
		printIt("      2. Assumed top of queues is on the left.");
		delay(500);
		printIt("      3. For evaluating expressions with parentheses");
		delay(700);
		printIt("        only 1 operator is allowed per pair of parentheses.");
		delay(700);
		printIt("      4. Linklist pointer is on the left side. Pointers are");
		delay(700);
		printIt("      in the form of array indices for easy understanding.");
		delay(700);
		printIt("Please DON'T go back to the main menu unless finished with evaluation. " );
		delay(500);
		printIt("Have fun! Process will start in.... " );
		delay(500);
		printIt("3... ");
		delay(500);
		printIt("2.. ");
		delay(500);
		printIt("1. " + newline);
		delay(500);
	}
	/*
	 * For the interdelay of
	 * printing sentences.
	 * */
	public void optionPaneForLog(){
		boolean tempAgain = true;
		while(tempAgain){
			try{
				String temp = JOptionPane.showInputDialog("Enter delay for inspecting log history(1000 == 1 sec):");
				JOptionPane.showMessageDialog(null ,"View your log history for "+ Integer.parseInt(temp)/1000 +  " seconds only." );
				delay(Integer.parseInt(temp));
				JOptionPane.showMessageDialog(this, "Conversion is finished. Evaluation will follow");
				tempAgain = false;
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "Sorry wrong input. Try again.");
				tempAgain = true;
			}
		}
	}
	/*
	 * For getting input for the sake of
	 * delay in processes.
	 * */
	public void optionPaneForEval(String message){
		boolean tempBool = true;
		while(tempBool){
			try{
				String temp = JOptionPane.showInputDialog(message).toString();	
				delayTime = Integer.parseInt(temp);
				tempBool = false;
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "Wrong input.Try again.");
				tempBool = true;
			}
		}
	}
	/*
	 * sets the delay time 
	 * in terms of ints.
	 */
	public void setDelayTime(int time){
		delayTime = time;
	}
	/*
	 * This returns an int of 
	 * delay time.
	 * */
	public int getDelayTime(){
		return delayTime;
	}
	/*
	 * This manages the textfield
	 * of entering the postfix.
	 * */
	private class ReadyHandler implements ActionListener{
		public void actionPerformed(ActionEvent event) {	
			 if(event.getSource()== enterTF){
				expression = enterTF.getText();
				char[] array = new char[100];
				array = expression.toCharArray();
				for(int i =0; i< array.length ;i++){
					if( (array[i] >= 'a') && (array[i] <= 'z') || ( (array[i] >= 'A') && (array[i] <= 'Z'))){
						JOptionPane.showMessageDialog(null, "You can't write or mix letters in expression.");
						done = false;
						break;
					}
					else{
						done = true;
					}
				}
				if(!done){
					expression = null;
					enterTF.setText(null);
					enterTF.setEditable(true);		
				}else{
					enterTF.setEditable(false);
					done = false;
					enterTF.requestFocus();
					gui.putEnterTF.setText(enterTF.getText());
					bufferSetter(enterTF.getText());
					optionPaneForEval("Enter desired delay(1000 = 1 sec):");
					start();
				}
			}
		}
	}
	
	/*
	 * This converts an infix 
	 * to postfix by storing the
	 * operators in a stack.
	 * */
	public void convertToPostfix(String infix) {  
	     while (parser.hasMoreTokens()) {
	    	 String token = parser.nextToken();        
	    	 justAChar = token.charAt(0); 
	         if ( (token.length() == 1) && postFunctions.isOper(justAChar) ) {    	    
	        	 while (!operatorStack.isEmpty() && !postFunctions.lowerPrec(((String)operatorStack.peek()).charAt(0), justAChar)) {
	        		 String oper = (String)operatorStack.pop();//pops operator from stack
	        		 gui.removeTextField(0, "up");
	        		 gui.changePosition(0, "up", '-');
	        		 postfix.append(" ").append(oper);
	        		 gui.putCurPostFixTF.setText(postfix.toString());
        			 printIt("put " + oper + " to postfix");
        			 
        			 delay(getDelayTime());
	              }
	        	 if (justAChar == ')') {
	  
	        		 if(!operatorStack.isEmpty()){// if
	        		 		String operator = (String)operatorStack.pop();
	        		 
	        		 		delay(getDelayTime());
	        		 		postfix.append(" ").append(operator);
	        		 		upStackOperatorList.remove(upStackOperatorList.size()-1);
	        		 		gui.removeTextField(0, "up");
	        		 		gui.changePosition(0, "up", '-');
	        		 		printIt("put " + operator + " to postfix" );
	        		 		delay(getDelayTime());
	        		 	}
	              }
	        	 else if(justAChar == '(');
	             else{
	            	  upStackOperatorList.add(token);
	            	  parsed.append(token);
	  		    	  printIt("parsed "+ parsed );
	            	  gui.changePosition(0, "up", '+');
	            	  gui.makeTextField(0, token, "up");
	            	  printIt("pushed " + token + " to upper stack");
	            	  operatorStack.push(token);
	       	          delay(getDelayTime());
	              }
	           }
	        else if (( postFunctions.isSpace(justAChar) && token.length() == 1) )
	        	continue;                                            
	        else {  // number
	        	parsed.append(token);
		    	printIt("parsed "+ parsed + newline);
		    	postfix.append(" ").append(token); 
		    	printIt("put " + token + " to postfix" );
	        }
	         delay(getDelayTime());
	         gui.putCurPostFixTF.setText(postfix.toString());
	    }
	    while (!operatorStack.isEmpty()){
	    	if(upStackOperatorList.size() >0){
	    		upStackOperatorList.remove(upStackOperatorList.size()-1);
	    		gui.removeTextField(0, "up");
	    		 gui.changePosition(0, "up", '-');
	    		delay(getDelayTime());
	    	}
	    	printIt("putting " + operatorStack.peek() + " to postfix");
	    	parsed.append(operatorStack.peek());
	    	delay(getDelayTime());
	    	postfix.append(" ").append((String)operatorStack.pop());
	    }
	    printIt("parsed " + parsed);
	    delay(getDelayTime());
	    if(gui.putCurPostFixTF.getText() != null){
	    	gui.putCurPostFixTF.setText(postfix.toString());
	    } 
	}
	/*
	 * Sets the log history's area to be empty
	 * with anything.
	 * */
	public void textWillBeNull(JTextArea area){
		if(area.getText() != null)
			area.setText(null);
	}
	/*
	 * This evaluates the postfix expression
	 *  by putting the numbers in a stack & 
	 *  then evaluating when the token 
	 *  is an operator.*/
	public double evaluate(String expr) {
		gui.redeclareGUI();
		parser2 = new StringTokenizer(expr);
	    while (parser2.hasMoreTokens()) {
	    	token = parser2.nextToken();
	    	evalParsed.append(token);
	    	printIt("parsed " + evalParsed );
	    	delay(getDelayTime());
	    	if (postFunctions.isOperator(token)) {
	    		op2 = ((Integer) evaluateStack.pop());
    			gui.removeTextField(0, "up");  
    			gui.changePosition(0, "up", '-');
    			printIt("popped ",upStackNumberList.get(upStackNumberList.size()-1).toString(), " in upper stack");
    			upStackNumberList.remove(upStackNumberList.size()-1);
    			delay(getDelayTime());
    			
    			op1 = ((Integer) evaluateStack.pop());
    			gui.removeTextField(0, "up");
    			gui.changePosition(0, "up", '-');
    			printIt("popped ", upStackNumberList.get(upStackNumberList.size()-1).toString()," in upper stack");
    			upStackNumberList.remove(upStackNumberList.size()-1);    
    			delay(getDelayTime());
	    	
	    		result = postFunctions.evalSingleOp(token.charAt(0), op1, op2);
	    		printIt(op1 + " and "+ op2 + "  are evaluated");
	    		evaluateStack.push(new Integer(result));
	    		upStackNumberList.add(result);
	    		if(!upStackNumberList.isEmpty()){
	               	gui.makeTextField(0, upStackNumberList.get(upStackNumberList.size()-1).toString(), "up");
	               	gui.changePosition(0, "up", '+');
	               	printIt("result "+ result+ " put to upper Stack" );
	               	delay(getDelayTime());
	    		}else{
	    			gui.makeTextField(0, upStackNumberList.get(0).toString(), "up");
	    			printIt("result "+ result+ " put to upper Stack");
	    			delay(getDelayTime());
	    		}    
	    	}else{
	    		upStackNumberList.add(new Integer(Integer.parseInt(token)));
	    		gui.makeTextField(0, upStackNumberList.get(upStackNumberList.size()-1).toString(), "up");
	    		gui.changePosition(0, "up", '+');
	    		printIt("enqueued " , upStackNumberList.get(upStackNumberList.size()-1).toString() , " upperstack"); 
	    		delay(getDelayTime());
	    		evaluateStack.push(new Integer(Integer.parseInt(token)));
	    			
	    	}
	    	delay(getDelayTime());
	    	gui.putCurPostFixTF.setText(resultBuffer.toString());
	    	delay(getDelayTime());
	    }
	    result = (Integer) evaluateStack.pop();
	    gui.removeTextField(0, "up");
	    gui.changePosition(0, "up", '-');
	    printIt("removed" , Integer.toString(result), " from the upper Stack");
	    upStackNumberList.remove(upStackNumberList.size()-1);
	    delay(getDelayTime());
	    gui.putCurPostFixTF.setText(Integer.toString(result));
	    return result;
	                
    }
	/*
	 * This sets the waiting time
	 *  before proceeding to
	 * current process.*/
	public void delay(int time){
		try{
			Thread.sleep(time);
		}catch(InterruptedException e){
			Thread.currentThread().interrupt();
		}
	}
	public void printIt( String message, String buffer, String destination){
  	  logJTA.append( message + buffer + destination + newline);
  	  logJTA.setCaretPosition(logJTA.getDocument().getLength());
	}
	public void printIt(String message){
		logJTA.append(message + newline);
		logJTA.setCaretPosition(logJTA.getDocument().getLength());
	}
	/*
	 * This is the thread start*/
	public void start(){
		stillRunning = true;
		thread = new Thread(this);
		thread.start();
	}
	/*This is the thread stop*/
	public synchronized void stop(){
		stillRunning = false;
	}
	/*painting of the background image.*/
	public void paintComponent(Graphics g){
		aboutImage = new ImageIcon(getClass().getResource("/verb2.jpg"));
		aboutImage.paintIcon(this,g,0,0);
	}
	/*This gathers all the GUI functions and its handlers.*/
	public void visual(){
		addOtherGuiStuff();
		addButtons();
		gui.addLabels();
		gui.addTextFields();
		ReadyHandler handler = new ReadyHandler();
		addListener(handler);	
	}
	/*Functions declare necessary stuff*/
	public void declarations(){
		expression= new String();
		done = false;
		stillRunning = false;
		gui = new GuiDeclarations(this);
		evaluateStack = new UpperStack<Integer>(this,gui);
		operatorStack = new UpperStack<String>(this,gui);
		upStackOperatorList = new ArrayList<String>();
		upStackNumberList = new ArrayList<Integer>();
		postFunctions = new PostfixFunctions();
	}
	/*Adds other gui components that can't
	 * be labeled in any other way.*/
	public void addOtherGuiStuff(){
		enterTF = new JTextField(15);
		enterTF.setBounds(340,10,400,30);
		enterTF.setBackground(Color.BLACK);
		enterTF.setForeground(Color.YELLOW);
		enterTF.setCaretColor(Color.YELLOW);
		enterTF.setText(null);
		enterTF.setEditable(true);
		logJTA = new JTextArea();
		logJTA.setWrapStyleWord(true);
		gui.textAreaProperties(logJTA,280,850,620,140,this);
		this.add(enterTF);
	}
	/*Adds the buttons in this panel.*/
	public void addButtons(){
		back = new JButton(new ImageIcon(getClass().getResource("/back-light.png")));
		back.setRolloverIcon(new ImageIcon(getClass().getResource("/back-dark.png")));
		back.setBounds(10, 5, 100, 50);
		gui.buttonProperties(back);
		this.add(back);
	}
	/* This sets the text areas to null.*/
	public void setComponentTextNull(){
		enterTF.setText(null);
		gui.putEnterTF.setText(null);
		gui.putCurPostFixTF.setText(null);
		logJTA.setText(null);
	}
	public void addListener(ActionListener listen){	
		back.addActionListener(listen);
		enterTF.addActionListener(listen);
	}
	public void bufferSetter(String infix){
		parser = new StringTokenizer(infix,"+-*/^() ",true);
		postfix = new StringBuffer(infix.length());  
		resultBuffer = new StringBuffer(infix.length());
		evalParsed = new StringBuffer();
		parsed = new StringBuffer();
	}
	
}
