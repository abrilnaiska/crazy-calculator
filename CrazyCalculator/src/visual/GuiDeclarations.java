package visual;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GuiDeclarations {
	 private JLabel enterJL;
	 private JLabel putEnterJL;
	 private JLabel putCurPostFixJL;
	 private JLabel upperStackJL;
	 private JLabel queueJL;
	 private JLabel lowerStackJL;
	 private JLabel udsLowerStackJL;
	 private JLabel upStackJL;
	 public JTextField putEnterTF;
	 public JTextField putCurPostFixTF;	  
	 public ArrayList<JTextField> upStackLeftTF;
	 private int xUpStack = 460;
	 private int yUpStack = 120;
	 public ArrayList<JTextField> queueSulodLeftTF;
	 private int xQueueSulod = 230;
	 private int yQueueSulod = 200;
	 public ArrayList<JTextField> queueTempLeftTF;
	 private int xQueueTemp = 680;
	 private int yQueueTemp = 200;
	 public ArrayList<JTextField> lowStackSulodLeftTF;
	 private int xLowStackSulod = 120;
	 private int yLowStackSulod = 280;
	 public ArrayList<JTextField> lowStackSulodTempLeftTF;
	 private int xLowStackSulodTemp = 340;
	 private int yLowStackSulodTemp = 280;
	 public ArrayList<JTextField> lowStackTempLeftTF;
	 private int xLowStackTemp = 570;
	 private int yLowStackTemp = 280;
	 public ArrayList<JTextField> lowStackTempTempLeftTF;
	 private int xLowStackTempTemp = 790;
	 private int yLowStackTempTemp = 280;
	 public ArrayList<JTextField> udsLowStackSulodTFInfo;
	 public ArrayList<JTextField> udsLowStackSulodTFLink;
	 private int xudsLowStackSulod = 80;
	 private int yudsLowStackSulod = 360;
	 private int yudsLowStackSulodLink = 390;
	 public ArrayList<JTextField> udsLowStackSulodTempTFInfo;
	 public ArrayList<JTextField> udsLowStackSulodTempTFLink;
	 private int xudsLowStackSulodTemp = 300;
	 private int yudsLowStackSulodTemp = 360;
	 private int yudsLowStackSulodTempLink = 390;
	 public ArrayList<JTextField> udsLowStackTempTFInfo;
	 public ArrayList<JTextField> udsLowStackTempTFLink;
	 private int xudsLowStackTemp = 540;
	 private int yudsLowStackTemp = 360;
	 private int yudsLowStackTempLink = 390;
	 public ArrayList<JTextField> udsLowStackTempTempTFInfo;
	 public ArrayList<JTextField> udsLowStackTempTempTFLink;
	 private int xudsLowStackTempTemp = 750;
	 private int yudsLowStackTempTemp = 360;
	 private int yudsLowStackTempTempLink = 390;
	 private JLabel queueMainJL;
	 private JLabel queueTempJL;
	 private JLabel lsMainJL;
	 private JLabel lsMainTempJL;
	 private JLabel lsTempJL;
	 private JLabel lsTempTempJL;
	 private JLabel udsMainJL;
	 private JLabel udsMainTempJL;
	 private JLabel udsTempJL;
	 private JLabel udsTempTempJL;
	 private JPanel panel;
	 public GuiDeclarations(JPanel panel){
		  this.panel = panel;
		  upStackLeftTF = new ArrayList<JTextField>();
		  queueSulodLeftTF = new ArrayList<JTextField>();
		  queueTempLeftTF = new ArrayList<JTextField>();
		  lowStackSulodLeftTF = new ArrayList<JTextField>();
		  lowStackSulodTempLeftTF = new ArrayList<JTextField>();
		  lowStackTempLeftTF = new ArrayList<JTextField>();
		  lowStackTempTempLeftTF = new ArrayList<JTextField>();
		  udsLowStackSulodTFLink = new ArrayList<JTextField>();
		  udsLowStackSulodTempTFLink = new ArrayList<JTextField>();
		  udsLowStackTempTFLink = new ArrayList<JTextField>();
		  udsLowStackTempTempTFLink = new ArrayList<JTextField>();
		  udsLowStackSulodTFInfo = new ArrayList<JTextField>();
		  udsLowStackSulodTempTFInfo = new ArrayList<JTextField>();
		  udsLowStackTempTFInfo = new ArrayList<JTextField>();
		  udsLowStackTempTempTFInfo = new ArrayList<JTextField>();
	  }
	  public void addLabels(){
		  enterJL = new JLabel();
		  putEnterJL = new JLabel();
		  putCurPostFixJL = new JLabel();
		  upperStackJL = new JLabel();
		  upStackJL = new JLabel();
		  queueJL = new JLabel();
		  lowerStackJL = new JLabel();
		  udsLowerStackJL = new JLabel();
		  queueMainJL = new JLabel();
		  queueTempJL = new JLabel();
		  lsMainJL = new JLabel();
		  lsTempJL = new JLabel();
		  udsMainJL = new JLabel();
		  udsTempJL = new JLabel();
		  lsMainTempJL = new JLabel();
		  lsTempTempJL = new JLabel();
		  udsMainTempJL = new JLabel();
		  udsTempTempJL = new JLabel();
		  labelProperties(enterJL, "Enter Mathematical (Infix) Expression", 430,32, 470, 45);
		  labelProperties(putEnterJL, "Expression: ",10, 50,100,70);
		  labelProperties(putCurPostFixJL, "Output: ", 650,50,400,70);
		  labelProperties(upperStackJL, "Stack",40,80,150,100);
		  labelProperties(upStackJL,"Top Stack",530, -40,200,300);
		  labelProperties(queueJL, "Queue",40,140,160,150);
		  labelProperties(lowerStackJL, "LowStack(LS)", 5, 110, 100, 300);
		  labelProperties(udsLowerStackJL, "UDS of LS", 5, 190, 250,300);
		  labelProperties(queueMainJL,"Main Q",310,40,200,300);
		  labelProperties(queueTempJL,"Temp Q",750,40,200,300);
		  labelProperties(lsMainJL,"LS Main",190,120,200,300);
		  labelProperties(lsMainTempJL,"LS MainTemp ",390,120,200,300);
		  labelProperties(udsMainJL,"UDS Main ",190,200,200,300);
		  labelProperties(udsMainTempJL,"UDS MainTemp ",390,200,200,300);
		  labelProperties(lsTempJL,"LSTemp",650,120,200,300);
		  labelProperties(lsTempTempJL,"LS Temptemp ",840,120,200,300);
		  labelProperties(udsTempJL,"UDS Temp ",640,200,200,300);	
		  labelProperties(udsTempTempJL,"UDs Temptemp ",840,200,200,300);	
	  }
	public void labelProperties(JLabel label, String text, int x, int y, int width, int height){
		label.setText(text);
		label.setBounds(x, y, width, height);
		label.setForeground(Color.white);
		label.setFocusable(false);
		panel.add(label);
	}
	public void addTextFields(){
		putEnterTF = new JTextField(15);
		textFieldProperties(putEnterTF,80,70,350,30,false);
		putCurPostFixTF = new JTextField(15);
		textFieldProperties(putCurPostFixTF,700,70,300,30,false);
		
	}
		/*Makes a textfield as a representation of a link in a LinkList*/
	public void makeTextField( int marker, String str, String type){
		if(type == "up"){
			if(marker == 0)
				singleAdd(upStackLeftTF,xUpStack, yUpStack,str);
		}
		if(type == "ls"){//if under lowerstack
			if(marker == 1)
				singleAdd(lowStackSulodLeftTF,xLowStackSulod, yLowStackSulod,str);
			else if( marker == 2)
				singleAdd(lowStackSulodTempLeftTF,xLowStackSulodTemp, yLowStackSulodTemp,str);
			else if(marker == 3)
				singleAdd(lowStackTempLeftTF,xLowStackTemp, yLowStackTemp,str);
			else if(marker == 4)
				singleAdd(lowStackTempTempLeftTF,xLowStackTempTemp, yLowStackTempTemp,str);
			
		}else if(type == "uds"){//if under uds
			if(marker == 1)//uds main
				singleAdd(udsLowStackSulodTFInfo,xudsLowStackSulod,yudsLowStackSulod,str);
			else if(marker == 2)
				singleAdd(udsLowStackSulodTempTFInfo, xudsLowStackSulodTemp, yudsLowStackSulodTemp,str);
			else if(marker == 3)
				singleAdd(udsLowStackTempTFInfo, xudsLowStackTemp, yudsLowStackTemp,str);
			else if(marker == 4)
				singleAdd(udsLowStackTempTempTFInfo, xudsLowStackTempTemp, yudsLowStackTempTemp,str);
			
		}else if(type == "q"){
			if(marker == 1)
				singleAdd(queueSulodLeftTF, xQueueSulod, yQueueSulod, str);
			else if(marker == 2){
				singleAdd(queueTempLeftTF, xQueueTemp, yQueueTemp, str);
		}
		}
	}
	public void makeLink(boolean hasFront, boolean hasBack, int type){ // 0-non 1-true
		if(hasFront){
			
			if(!hasBack){// last
				if(type == 1){
					if(!udsLowStackSulodTFLink.isEmpty()){
						udsLowStackSulodTFLink.get(udsLowStackSulodTFLink.size()-1).setText(String.valueOf(udsLowStackSulodTFInfo.indexOf(udsLowStackSulodTFInfo.get(udsLowStackSulodTFInfo.size()-1))));
					}				
					addLink(udsLowStackSulodTFLink, xudsLowStackSulod-20, yudsLowStackSulodLink,"null");
				}else if(type == 2){
					if(!udsLowStackSulodTempTFLink.isEmpty()){
						udsLowStackSulodTempTFLink.get(udsLowStackSulodTempTFLink.size()-1).setText(String.valueOf(udsLowStackSulodTempTFInfo.indexOf(udsLowStackSulodTempTFInfo.get(udsLowStackSulodTempTFInfo.size()-1))));
					}
						addLink(udsLowStackSulodTempTFLink, xudsLowStackSulodTemp-20, yudsLowStackSulodTempLink,"null");
				}else if(type == 3){
					if(!udsLowStackTempTFLink.isEmpty()){
						udsLowStackTempTFLink.get(udsLowStackTempTFLink.size()-1).setText(String.valueOf(udsLowStackTempTFInfo.indexOf(udsLowStackTempTFInfo.get(udsLowStackTempTFInfo.size()-1))));
					}
					addLink(udsLowStackTempTFLink, xudsLowStackTemp-20, yudsLowStackTempLink,"null");
				}else if(type == 4){
					if(!udsLowStackTempTempTFLink.isEmpty()){
						udsLowStackTempTempTFLink.get(udsLowStackTempTempTFLink.size()-1).setText(String.valueOf(udsLowStackTempTempTFInfo.indexOf(udsLowStackTempTempTFInfo.get(udsLowStackTempTempTFInfo.size()-1))));
					}
					addLink(udsLowStackTempTempTFLink, xudsLowStackTempTemp-20, yudsLowStackTempTempLink,"null");
				}
			}
		}else{
			if(!hasBack){
				if(type == 1){
					addLink(udsLowStackSulodTFLink, xudsLowStackSulod-20, yudsLowStackSulodLink,
							"null");
				}else if(type == 2){
					addLink(udsLowStackSulodTempTFLink, xudsLowStackSulodTemp-20, yudsLowStackSulodTempLink,
							"null");
				}else if(type == 3){
					addLink(udsLowStackTempTFLink, xudsLowStackTemp-20, yudsLowStackTempLink,
							"null");
				}else if(type == 4){
					addLink(udsLowStackTempTempTFLink, xudsLowStackTempTemp-20, yudsLowStackTempTempLink,
							"null");
				}
			}
		}
		
	}
	public void setLinkText(int ifDelAfter, int type){// 1-del last, either 1 it size, or size()-1
		if(ifDelAfter == 1){//size == 1
			if(type == 1){
				udsLowStackSulodTFLink.get(0).setText("null");
			}else if(type == 2){
				udsLowStackSulodTempTFLink.get(0).setText("null");
			}else if(type == 3){
				udsLowStackTempTFLink.get(0).setText("null");
			}else if(type == 4){
				udsLowStackTempTempTFLink.get(0).setText("null");
			}
		}else{//size  > 1
			if(type == 1){
				udsLowStackSulodTFLink.get(udsLowStackSulodTFLink.size()-1).setText(udsLowStackSulodTFLink.size()-1 + "");
			}else if(type == 2){
				udsLowStackSulodTempTFLink.get(udsLowStackSulodTempTFLink.size()-1).setText(udsLowStackSulodTempTFLink.size()-1 + "");
			}else if(type == 3){
				udsLowStackTempTFLink.get(udsLowStackTempTFLink.size()-1).setText(udsLowStackTempTFLink.size()-1 + "");
			}else if(type == 4){
				udsLowStackTempTempTFLink.get(udsLowStackTempTempTFLink.size()-1).setText(udsLowStackTempTempTFLink.size()-1 + "");
			}
		}
		
	}
	public void singleAdd(ArrayList<JTextField> field, int x, int y, String str ){
		JTextField store = new JTextField(15);
		field.add(store);	
		textFieldProperties(field.get(field.size()-1), x, y, 35,30, true);
		x += 40;
		field.get(field.size()-1).setText(str);
		
	}
	public void addLink(ArrayList<JTextField> field, int x, int y, String str){
		JTextField store = new JTextField(15);
		field.add(store);	
		textFieldLinkProperties(field.get(field.size()-1), x, y, 30,20, true);
		x += 35;
		field.get(field.size()-1).setText(str);
	}
	public void singleDelete(ArrayList<JTextField> field, int x, int type, int mark){
		if(mark == 2){//normal
			if(!field.isEmpty()){
				field.get(0).setVisible(false);
				field.remove(0);
				
			}
		}
		else if(mark == 3){//q
			if(!field.isEmpty()){
				field.get(field.size()-1).setVisible(false);
				field.remove(field.size()-1);
			}
		}
	}
	public void adjustTF(ArrayList<JTextField> field, char operator, int type){
		if(type == 1){//for upper stack, queue, lower queue
			for(int i = 0; i < field.size();i++){
				//sets the previous element to next element's position
				if(field.get(i).getText() == null){
					field.get(i).setVisible(false);
					field.remove(field.get(i));
					if(operator == '+'){
						field.get(i).setBounds(field.get(i).getBounds().x,//+40
								field.get(i).getBounds().y,field.get(i).getBounds().width
								,field.get(i).getBounds().height);
					}else if(operator == '-'){
						field.get(i).setBounds(field.get(i).getBounds().x,
								field.get(i).getBounds().y,field.get(i).getBounds().width
								,field.get(i).getBounds().height);
					}
				}else{
					field.get(i).setVisible(true);
					if(field.size()== 0){//if size == 1
						if(operator == '+'){
							field.get(i).setBounds(field.get(i).getBounds().x,//+40
									field.get(i).getBounds().y,field.get(i).getBounds().width
									,field.get(i).getBounds().height);
						}else if(operator == '-'){
							field.get(i).setBounds(field.get(i).getBounds().x,
									field.get(i).getBounds().y,field.get(i).getBounds().width
									,field.get(i).getBounds().height);
						}
					}else{
						if(operator == '+'){
							field.get(i).setBounds(field.get(i).getBounds().x+35,
									field.get(i).getBounds().y,field.get(i).getBounds().width
									,field.get(i).getBounds().height);
						}else if(operator == '-'){
							field.get(i).setBounds(field.get(i).getBounds().x-35,
									field.get(i).getBounds().y,field.get(i).getBounds().width
									,field.get(i).getBounds().height);
						}
					}
				}
			}
		}else {//for uds
			for(int i = 0; i < field.size();i++){
				//sets the previous element to next element's position
				field.get(i).setVisible(true);
				if(operator == '+'){
					if(field.size() == 0){ // 1
						field.get(i).setBounds(field.get(i).getBounds().x,//+35
								field.get(i).getBounds().y,field.get(i).getBounds().width
								,field.get(i).getBounds().height);
						field.get(i).setText("null");
					}else{//interchange
						field.get(i).setBounds(field.get(i).getBounds().x+35,
								field.get(i).getBounds().y,field.get(i).getBounds().width
								,field.get(i).getBounds().height);
						
					}
				}else if(operator == '-'){
					if(field.size() == 0){ //1
						field.get(i).setBounds(field.get(i).getBounds().x,
								field.get(i).getBounds().y,field.get(i).getBounds().width
								,field.get(i).getBounds().height);	
						field.get(i).setText("null");
					}else{
						field.get(i).setBounds(field.get(i).getBounds().x-35,
								field.get(i).getBounds().y,field.get(i).getBounds().width
								,field.get(i).getBounds().height);
					}
				}
			}
		}
	}
	public void redeclareGUI(){
		
		//storages of data
		upStackLeftTF = new ArrayList<JTextField>();
		queueSulodLeftTF = new ArrayList<JTextField>();
		queueTempLeftTF = new ArrayList<JTextField>();
		lowStackSulodLeftTF = new ArrayList<JTextField>();
		lowStackSulodTempLeftTF = new ArrayList<JTextField>();
		lowStackTempLeftTF = new ArrayList<JTextField>();
		lowStackTempTempLeftTF = new ArrayList<JTextField>();
		udsLowStackSulodTFLink = new ArrayList<JTextField>();
		udsLowStackSulodTempTFLink = new ArrayList<JTextField>();
		udsLowStackTempTFLink = new ArrayList<JTextField>();
		udsLowStackTempTempTFLink = new ArrayList<JTextField>();
		udsLowStackSulodTFInfo = new ArrayList<JTextField>();
		udsLowStackSulodTempTFInfo = new ArrayList<JTextField>();
		udsLowStackTempTFInfo = new ArrayList<JTextField>();
		udsLowStackTempTempTFInfo = new ArrayList<JTextField>();
		
		//positions
		xUpStack = 460;
		yUpStack = 120;
		xQueueSulod = 230;
		yQueueSulod = 200;
		xQueueTemp = 680;
		yQueueTemp = 200;
		xLowStackSulod = 120;
		yLowStackSulod = 280;
		xLowStackSulodTemp = 340;
		yLowStackSulodTemp = 280;
		xLowStackTemp = 570;
		yLowStackTemp = 280;
		xLowStackTempTemp = 790;
		yLowStackTempTemp = 280;
		xudsLowStackSulod = 80;
		yudsLowStackSulod = 360;
		yudsLowStackSulodLink = 390;
		xudsLowStackSulodTemp = 300;
		yudsLowStackSulodTemp = 360;
		yudsLowStackSulodTempLink = 390;
		xudsLowStackTemp = 540;
		yudsLowStackTemp = 360;
		yudsLowStackTempLink = 390;
		xudsLowStackTempTemp = 750;
		yudsLowStackTempTemp = 360;
		yudsLowStackTempTempLink = 390;
	}
	public void changePosition(int marker, String type, char oper){
		if(oper == '+'){
			if(type == "up"){
				if(marker == 0){
					adjustTF(upStackLeftTF, '+',1);
				}
			}
			if(type == "ls"){
				if(marker == 1){
					adjustTF(lowStackSulodLeftTF, '+',1);
				}else if(marker == 2){
					adjustTF(lowStackSulodTempLeftTF, '+',1);
				}else if(marker == 3){
					adjustTF(lowStackTempLeftTF, '+',1);
				}else if(marker == 4){
					adjustTF(lowStackTempTempLeftTF,'+',1);
				}
			}else if(type == "uds"){
				if(marker == 1){
					adjustTF(udsLowStackSulodTFInfo, '+',1);
					adjustTF(udsLowStackSulodTFLink, '+',0);
				}else if( marker == 2){
					adjustTF(udsLowStackSulodTempTFInfo, '+',1);
					adjustTF(udsLowStackSulodTempTFLink, '+',0);
				}else if( marker == 3){
					adjustTF(udsLowStackTempTFInfo, '+',1);	
					adjustTF(udsLowStackTempTFLink, '+',0);	
				}else if(marker == 4){
					adjustTF( udsLowStackTempTempTFInfo, '+',1);
					adjustTF( udsLowStackTempTempTFLink, '+',0);
				}
			}else if(type == "q"){
				if(marker == 1){
					adjustTF( queueSulodLeftTF, '+',1);	
				}else if(marker == 2){
					adjustTF( queueTempLeftTF, '+',1);
				}
			}
		}else if(oper == '-'){
			if(type == "up"){
				if(marker == 0){
					adjustTF(upStackLeftTF, '-',1);
				}
			}
			if(type == "ls"){
				if(marker == 1){
					adjustTF(lowStackSulodLeftTF, '-',1);
				}else if(marker == 2){
					adjustTF(lowStackSulodTempLeftTF, '-',1);
				}else if(marker == 3){
					adjustTF(lowStackTempLeftTF, '-',1);
				}else if(marker == 4){
					adjustTF(lowStackTempTempLeftTF,'-',1);
				}
			}else if(type == "uds"){
				if(marker == 1){
					adjustTF(udsLowStackSulodTFInfo, '-',1);
					adjustTF(udsLowStackSulodTFLink, '-',0);
				}else if( marker == 2){
					adjustTF(udsLowStackSulodTempTFInfo, '-',1);
					adjustTF(udsLowStackSulodTempTFLink, '-',0);
				}else if( marker == 3){
					adjustTF(udsLowStackTempTFInfo, '-',1);	
					adjustTF(udsLowStackTempTFLink, '-',0);	
				}else if(marker == 4){
					adjustTF( udsLowStackTempTempTFInfo, '-',1);
					adjustTF( udsLowStackTempTempTFLink, '-',0);
				}
			}else if(type == "q"){
				if(marker == 1){
					adjustTF( queueSulodLeftTF, '-',1);	
				}else if(marker == 2){
					adjustTF( queueTempLeftTF, '-',1);
				}
			}
		}
	}
	
	public void removeTextField( int marker, String type){
		if(type == "up"){
			if(marker == 0){
				singleDelete(upStackLeftTF,xUpStack,0,3);
			}
		}
		if(type == "ls"){
			if(marker == 1){
				singleDelete(lowStackSulodLeftTF,xLowStackSulod,0,3);//2
			}else if(marker == 2){
				singleDelete(lowStackSulodTempLeftTF, xLowStackSulodTemp,0,3);//2
			}else if(marker == 3){
				singleDelete(lowStackTempLeftTF,xLowStackTemp,0,3);//2
			}else if(marker == 4){
				singleDelete(lowStackTempTempLeftTF,xLowStackTempTemp,0,3);//2
			}
		}else if(type == "uds"){
			if(marker == 1){//tanan ini 2
				singleDelete(udsLowStackSulodTFInfo, xudsLowStackSulod,0,3);
				singleDelete(udsLowStackSulodTFLink, xudsLowStackSulod,1,3);
			}else if( marker == 2){
				singleDelete(udsLowStackSulodTempTFInfo, xudsLowStackSulodTemp,0,3);
				singleDelete(udsLowStackSulodTempTFLink, xudsLowStackSulodTemp,1,3);
			}else if( marker == 3){
				singleDelete(udsLowStackTempTFInfo,xudsLowStackTemp,0,3);
				singleDelete(udsLowStackTempTFLink,xudsLowStackTemp,1,3);
			}else if(marker == 4){
				singleDelete(udsLowStackTempTempTFInfo,xudsLowStackTempTemp,0,3);
				singleDelete(udsLowStackTempTempTFLink,xudsLowStackTempTemp,1,3);
			}
		}else if(type == "q"){
			if(marker == 1){
				singleDelete(queueSulodLeftTF,xQueueSulod,0,2);//3
			}else if(marker == 2){
				singleDelete(queueTempLeftTF,xQueueTemp,0,2);//3
			}
		}
	
	}
	public void textFieldProperties(JTextField textField,int x, int y, int width, int height, boolean editable ){
		textField.setBounds(x, y, width, height);
		textField.setBackground(Color.BLACK);
		textField.setForeground(Color.YELLOW);
		textField.setCaretColor(Color.YELLOW);
		textField.setEditable(editable);
		textField.setVisible(true);
		panel.add(textField);
	}
	public void textFieldLinkProperties(JTextField textField,int x, int y, int width, int height, boolean editable ){
		textField.setBounds(x, y, width, height);
		textField.setBackground(Color.WHITE);
		textField.setForeground(Color.BLACK);
		textField.setCaretColor(Color.BLACK);
		textField.setEditable(editable);
		textField.setVisible(true);
		panel.add(textField);
	}
	
	public void textAreaProperties( JTextArea area, int x, int y, int width, int height, JPanel panel){
		area.setBounds(x,y, width, height);
		area.setCaretColor(Color.YELLOW);
		area.setBackground(Color.BLACK);
		area.setForeground(Color.pink);
		area.setText(null);	
		area.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(area);
		scrollPane.setBounds(240, 440, 580, 150);
	    GridBagConstraints c = new GridBagConstraints();
	    c.gridwidth = GridBagConstraints.REMAINDER;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.fill = GridBagConstraints.BOTH;
	    c.weightx = 1.0;
	    c.weighty = 50.0;
		panel.add(scrollPane, c);
	}
	public void buttonProperties(JButton button){
		button.setBorder(null);
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.setFocusable(false);
	}
		
		
}