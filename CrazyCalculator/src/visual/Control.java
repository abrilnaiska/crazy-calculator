package visual;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import controller.VerbVisTrial;


public class Control {
	public View view;
	public Menu menu;
	public AboutVisual aboutVis;
	public VerbVisTrial trial;
	public Control(View view, Menu menu,   VerbVisTrial trial, AboutVisual aboutVis){	
		this.view = view;
		this.menu = menu;
		this.aboutVis = aboutVis;
		this.trial = trial;
		this.menu.addListener(new ButtonListener());
		this.aboutVis.addListener(new ButtonListener());
		this.trial.addListener(new ButtonListener());
	}
	
	class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			try{
				if(event.getSource() == menu.aboutJB){				
					view.Switch("AboutVisual");
				}else if(event.getSource() == menu.verboseJB){
					
					view.Switch("VerbVisTrial");
					view.verb.setComponentTextNull();
					view.verb.enterTF.setEditable(true);
					
					
				}else if(event.getSource() == aboutVis.back){
					view.verb.stop();
					view.Switch("Menu");
					
				}else if(event.getSource() == trial.back){
					
					view.Switch("Menu");
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(menu, "Sorry something went wrong.");
			}
		}	
	}
}
