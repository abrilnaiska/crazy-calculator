package visual;
/*This is for the MENU BUTTONS.*/
import java.awt.*;

import javax.swing.*;

import java.awt.event.ActionListener;


public class Menu extends JPanel {
	
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private View view;
	private ImageIcon background;
	public JButton aboutJB;
	public JButton verboseJB;
	private ImageIcon titleImage;
	public Menu(View view){
		
		this.view = view;
		setLayout(null);
		setOpaque(false);
		
		aboutJB = new JButton(new ImageIcon(getClass().getResource("/about-light.png")));
		aboutJB.setRolloverIcon(new ImageIcon(getClass().getResource("/about-dark.png")));
		aboutJB.setBounds(400,250,207,150);
		buttonSettings(aboutJB);
		add(aboutJB);
		verboseJB = new JButton(new ImageIcon(getClass().getResource("/convert-light.png")));
		verboseJB.setRolloverIcon(new ImageIcon(getClass().getResource("/convert-dark.png")));
		verboseJB.setBounds(400,360,207,150);
		buttonSettings(verboseJB);
		add(verboseJB);
		
	}
	
	public void buttonSettings(JButton button){
		
		button.setBorder(null);
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.setFocusable(false);
	}
	
	public void paintComponent(Graphics g){
		background = new ImageIcon(getClass().getResource("/CCmain.jpg"));
		background.paintIcon(this,g,0,0);
		titleImage= new ImageIcon(getClass().getResource("/title.png")); 
		titleImage.paintIcon(this, g,260, 80);
	}
	
	public void addListener(ActionListener listen){
		aboutJB.addActionListener(listen);
		verboseJB.addActionListener(listen);
	}
}
