package visual;
import java.awt.*;

import javax.swing.*;

import controller.VerbVisTrial;



public class View extends JFrame {
	
	private static final long serialVersionUID = 1L;
	public Menu menu;
	public AboutVisual aboutVisual;
	public VerbVisTrial verb;
	private CardLayout layout;
	private JPanel panel;
	
	public View(){
		
		super("A Crazy Calculator");
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1015, 625);
		setLocationRelativeTo(null);
		setResizable(false);
		
		menu = new Menu(this);
		aboutVisual = new AboutVisual(this);
		verb = new VerbVisTrial(this);
		panelProperties(menu);
		panelProperties(aboutVisual);
		panelProperties( verb);
		layout = new CardLayout();
		panel = new JPanel();
		panel.setBounds(0,0,1015,625);
		panel.setLayout(layout);
		panel.add(menu, "Menu");
		panel.add(aboutVisual, "AboutVisual");
		panel.add(verb, "VerbVisTrial");
		add(panel);
		setVisible(true);
	}
	public void panelProperties(JPanel panel){
		panel.setBounds(0,0,1015,625);
		panel.setFocusable(false);
	}
	public void Switch(String str){
		layout.show(panel, str);
	}
}

