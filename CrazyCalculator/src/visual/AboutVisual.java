package visual;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class AboutVisual extends JPanel {
	
	private static final long serialVersionUID = 1L;
	public JButton back;
	private ImageIcon aboutImage;
	private ImageIcon aboutHeader;
	private ImageIcon instructions;
	@SuppressWarnings("unused")
	private View view;
	public AboutVisual(View view){
		this.view = view;
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,800,600);
	
		addButtons();
	
	}
	public void paintComponent(Graphics g){
		aboutImage = new ImageIcon(getClass().getResource("/verb2.jpg"));
		aboutImage.paintIcon(this,g,0,0);
		aboutHeader = new ImageIcon(getClass().getResource("/aboutHeader.png"));
		aboutHeader.paintIcon(this, g, 380,25);
		instructions = new ImageIcon(getClass().getResource("/sulod.png"));
		instructions.paintIcon(this, g, 270, 140);
		
	}
	public void addButtons(){
		back = new JButton(new ImageIcon(getClass().getResource("/back-light.png")));
		back.setRolloverIcon(new ImageIcon(getClass().getResource("/back-dark.png")));
		back.setBounds(700,450,427,200);
		buttonProperties(back);
		this.add(back);
	}
	public void buttonProperties(JButton button){
		button.setBorder(null);
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.setFocusable(false);
	}
	
	public void labelProperties(JLabel label, int x, int y, int width, int height){
		label.setForeground(Color.YELLOW);
		label.setBounds(x, y, width, height);
		
	}
	public void addListener(ActionListener listen){	
		back.addActionListener(listen);
	}
}
